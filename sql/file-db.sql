CREATE TABLE `sys_file_detail` (
  `id` bigint(20) NOT NULL,
  `system_code` varchar(20) NOT NULL COMMENT '系统名称',
  `file_name` varchar(150) DEFAULT NULL COMMENT '源文件名称',
  `file_path` varchar(255) DEFAULT NULL COMMENT '保存路径',
  `format` varchar(5) DEFAULT NULL COMMENT '文件格式',
  `file_size` varchar(50) DEFAULT NULL COMMENT '源文件大小',
  `file_unique_id` varchar(100) NOT NULL COMMENT '源文件id',
  `file_fdfs_id` varchar(100) NOT NULL COMMENT '源文件fdfs id',
  `disabled` tinyint(4) NOT NULL DEFAULT '0' COMMENT '删除标识 0:可用 1：禁用',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `delete_time` datetime DEFAULT NULL COMMENT '删除时间',
  PRIMARY KEY (`id`),
  KEY `idx_file_detail_file_unique_id` (`file_unique_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;



CREATE TABLE `sys_file_share` (
  `file_unique_id` varchar(100) NOT NULL,
  `share_system_code` bigint(20) DEFAULT NULL,
  KEY `idx_fileshare_file_unique_id` (`file_unique_id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4;

CREATE TABLE `sys_register_system` (
  `id` bigint(20) NOT NULL COMMENT '唯一标识',
  `system_code` varchar(20) DEFAULT NULL COMMENT '系统名称',
  `system_name` varchar(100) DEFAULT NULL COMMENT '系统描述',
  `private_key` varchar(50) DEFAULT NULL COMMENT '认证Key',
  `manager_email` varchar(50) DEFAULT NULL COMMENT '负责人邮箱',
  `manager_phone` varchar(20) DEFAULT NULL COMMENT '负责人手机',
  `disabled` tinyint(4) DEFAULT NULL COMMENT '删除标识',
  `create_user` varchar(20) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_user` varchar(20) DEFAULT NULL COMMENT '修改人',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


