package org.artofsolving.jodconverter.document;

public enum DocumentFamily {

    TEXT, SPREADSHEET, PRESENTATION, DRAWING

}
