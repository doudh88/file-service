package org.artofsolving.jodconverter.office;

public class ExternalOfficeManagerConfiguration {

    private OfficeConnectionProtocol connectionProtocol = OfficeConnectionProtocol.SOCKET;
    private int portNumber = 2002;
    private String pipeName = "office";
    private boolean connectOnStart = true;

    public ExternalOfficeManagerConfiguration setConnectionProtocol(OfficeConnectionProtocol connectionProtocol) {
        this.connectionProtocol = connectionProtocol;
        return this;
    }

    public ExternalOfficeManagerConfiguration setPortNumber(int portNumber) {
        this.portNumber = portNumber;
        return this;
    }

    public ExternalOfficeManagerConfiguration setPipeName(String pipeName) {
        this.pipeName = pipeName;
        return this;
    }

    public ExternalOfficeManagerConfiguration setConnectOnStart(boolean connectOnStart) {
        this.connectOnStart = connectOnStart;
        return this;
    }

    public OfficeManager buildOfficeManager() {
        UnoUrl unoUrl =
            connectionProtocol == OfficeConnectionProtocol.SOCKET ? UnoUrl.socket(portNumber) : UnoUrl.pipe(pipeName);
        return new ExternalOfficeManager(unoUrl, connectOnStart);
    }

}
