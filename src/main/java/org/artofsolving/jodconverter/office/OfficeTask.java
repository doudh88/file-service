package org.artofsolving.jodconverter.office;

public interface OfficeTask {
    void execute(OfficeContext context) throws OfficeException;

}
