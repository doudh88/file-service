package org.artofsolving.jodconverter.office;

import java.util.EventListener;

interface OfficeConnectionEventListener extends EventListener {

    void connected(OfficeConnectionEvent event);

    void disconnected(OfficeConnectionEvent event);

}
