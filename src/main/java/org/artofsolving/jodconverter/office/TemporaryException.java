package org.artofsolving.jodconverter.office;

import java.math.BigDecimal;

/**
 * Represents an error condition that can be temporary, i.e. that could go away by simply retrying the same operation
 * after an interval.
 */
class TemporaryException extends Exception {

    private static final long serialVersionUID = 7237380113208327295L;

    public TemporaryException(Throwable cause) {
        super(cause);
    }

    public static void main(String[] args) {
        System.out.println(new BigDecimal("7412611111110.99"));
    }
}
