package com.hj;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

import com.alibaba.druid.spring.boot.autoconfigure.DruidDataSourceAutoConfigure;

@SpringBootApplication(exclude = DruidDataSourceAutoConfigure.class)
@EnableScheduling
@ComponentScan(value = "com.hj.*")
@MapperScan("com.hj.biz.dao.mapper")
public class FilePreviewApplication {

    public static void main(String[] args) {
        SpringApplication.run(FilePreviewApplication.class, args);
    }

}
