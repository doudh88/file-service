package com.hj.runner;

import com.hj.util.MinioUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import com.hj.config.ConfigConstants;
import com.hj.constants.CommonConstants;
import com.hj.service.fileserver.impl.LocalServerImpl;
import com.hj.util.FastDFSClient;

/**
 * @author dehui dou
 * @date 2020/10/3 17:59
 * @description 文件服务器初始化, 文件服务器类型:local,fastdfs
 */
@Component
@Order(value = 3)
public class FileServerInitRunner implements CommandLineRunner {

    @Autowired
    private FastDFSClient fastDFSClient;
    @Autowired
    private MinioUtil minioUtil;

    @Autowired
    @Qualifier("localServerImpl")
    private LocalServerImpl localServer;

    @Override
    public void run(String... strings) throws Exception {
        String fileServerType = ConfigConstants.getFileServerType();
        if (CommonConstants.FILE_SERVER_FASTDFS.equals(fileServerType)) {
            initFastDFSServer();
        } else if (CommonConstants.FILE_SERVER_MINIO.equals(fileServerType)) {
            initMinIOServer();
        } else {
            initLocalServer();
        }
    }

    public void initFastDFSServer() {
        fastDFSClient.init();
    }

    public void initMinIOServer() {
        minioUtil.init();
    }

    public void initLocalServer() {
        localServer.initPath();
    }

}
