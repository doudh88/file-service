package com.hj.runner;

import java.io.File;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * @author dehui dou
 * @date 2020/10/3 18:00
 * @description 文件预览目录创建
 */
@Component
@Order(value = 2)
public class FileViewPathInitRunner implements CommandLineRunner {

    @Value("${fileview.dir}")
    private String fileviewdir;

    @Override
    public void run(String... strings) throws Exception {
        File file = new File(fileviewdir);
        if (!file.exists()) {
            file.mkdirs();
        }
    }
}
