package com.hj.constants;

/**
 * @author dehui dou
 * @date 2020/10/3 17:46
 * @description 枚举基础类
 */
public interface BaseEnum {

    /*
     * 返回枚举的int数值
     */
    int intValue();

    /*
     * 返回枚举的字符值
     */
    String stringValue();
}