package com.hj.constants;

/**
 * @author dehui dou
 * @date 2020/10/3 17:47
 * @description 常量配置
 */
public interface CommonConstants {
    /*
     * 文件服务器类型
     */
    String FILE_SERVER_LOCAL = "local";
    String FILE_SERVER_FASTDFS = "fastdfs";
    String FILE_SERVER_MINIO = "minio";

    /*
     * 认证信息
     */
    String SYSTEM_CODE = "systemCode";
    String PRIVATE_KEY = "privateKey";
    String AUTHORIZATION = "Authorization";

    /**
     * @author dehui dou
     * @description 有效标识枚举
     * @date 2020/9/24 18:56
     * @params
     */
    enum CommonStatusEnum implements BaseEnum {
        ENABLED(0, "有效"), UNABLED(1, "删除");

        private int id;
        private String name;

        CommonStatusEnum(int id, String name) {
            this.id = id;
            this.name = name;
        }

        @Override
        public int intValue() {
            return this.id;
        }

        @Override
        public String stringValue() {
            return this.name;
        }
    }

}
