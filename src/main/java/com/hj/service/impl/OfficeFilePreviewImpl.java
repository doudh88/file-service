package com.hj.service.impl;

import com.hj.config.ConfigConstants;
import com.hj.model.FileAttribute;
import com.hj.service.FilePreview;
import com.hj.util.DownloadUtils;
import com.hj.util.FileUtils;
import com.hj.util.OfficeToPdf;
import com.hj.util.PdfUtils;
import com.hj.web.filter.BaseUrlFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;

import java.io.File;
import java.util.List;

/**
 * @author dehui dou
 * @date 2020/10/3 19:16
 * @description office类型处理实现类
 */
@Service
public class OfficeFilePreviewImpl implements FilePreview {

    @Autowired
    private PdfUtils pdfUtils;
    @Autowired
    private DownloadUtils downloadUtils;
    @Autowired
    private OfficeToPdf officeToPdf;

    public static final String OFFICE_PREVIEW_TYPE_IMAGE = "image";
    public static final String OFFICE_PREVIEW_TYPE_ALL_IMAGES = "allImages";

    /**
     * @author dehui dou
     * @description 文件预览处理实现
     * @param model
     * @param fileAttribute
     * @return java.lang.String
     */
    @Override
    public String filePreviewHandle(Model model, FileAttribute fileAttribute) {
        String fileName = fileAttribute.getFileViewName();
        // 预览Type，参数传了就取参数的，没传取系统默认
        String officePreviewType = model.asMap().get("officePreviewType") == null ? ConfigConstants.getOfficePreviewType() : model.asMap().get("officePreviewType").toString();
        String baseUrl = BaseUrlFilter.getBaseUrl();
        String suffix = fileName.substring(fileName.lastIndexOf(".") + 1);

        String filePath = String.format(ConfigConstants.getFileDir() + File.separator + fileName);
        boolean isHtml = suffix.equalsIgnoreCase("xls") || suffix.equalsIgnoreCase("xlsx");
        String pdfName = fileName.substring(0, fileName.lastIndexOf(".") + 1) + (isHtml ? "html" : "pdf");
        String outFilePath = ConfigConstants.getFileDir() + pdfName;
        if (StringUtils.hasText(outFilePath)) {
            officeToPdf.openOfficeToPDF(filePath, outFilePath);
            if (isHtml) {
                // 对转换后的文件进行操作(改变编码方式)
                FileUtils.doActionConvertedFile(outFilePath);
            }
        }
        if (!isHtml && baseUrl != null && (OFFICE_PREVIEW_TYPE_IMAGE.equals(officePreviewType) || OFFICE_PREVIEW_TYPE_ALL_IMAGES.equals(officePreviewType))) {
            return getPreviewType(model, "." + suffix, officePreviewType, baseUrl, pdfName, outFilePath, pdfUtils, OFFICE_PREVIEW_TYPE_IMAGE);
        }
        model.addAttribute("pdfUrl", pdfName);
        return isHtml ? "html" : "pdf";
    }


    static String getPreviewType(Model model, String suffix, String officePreviewType, String baseUrl, String pdfName, String outFilePath, PdfUtils pdfUtils, String officePreviewTypeImage) {
        List<String> imageUrls = pdfUtils.pdf2jpg(outFilePath, pdfName, baseUrl);
        if (imageUrls == null || imageUrls.size() < 1) {
            model.addAttribute("msg", "office转图片异常，请联系管理员");
            model.addAttribute("fileType", suffix);
            return "fileNotSupported";
        }
        model.addAttribute("imgurls", imageUrls);
        model.addAttribute("currentUrl", imageUrls.get(0));
        if (officePreviewTypeImage.equals(officePreviewType)) {
            return "officePicture";
        } else {
            return "picture";
        }
    }
}
