package com.hj.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import com.hj.model.FileAttribute;
import com.hj.service.FilePreview;

/**
 * @author dehui dou
 * @date 2020/10/3 19:16
 * @description office类型处理实现类
 */
@Service
public class OtherFilePreviewImpl implements FilePreview {

    /**
     * @author dehui dou
     * @description 文件预览处理实现
     * @param model
     * @param fileAttribute
     * @return java.lang.String
     */
    @Override
    public String filePreviewHandle(Model model, FileAttribute fileAttribute) {
        model.addAttribute("msg", "系统还不支持该格式文件的在线预览");
        return "fileNotSupported";
    }
}
