package com.hj.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import com.google.common.collect.Lists;
import com.hj.model.FileAttribute;
import com.hj.service.FilePreview;

/**
 * @author dehui dou
 * @date 2020/10/3 19:16
 * @description office类型处理实现类
 */
@Service
public class PictureFilePreviewImpl implements FilePreview {

    /**
     * @author dehui dou
     * @description 文件预览处理实现
     * @param model
     * @param fileAttribute
     * @return java.lang.String
     */
    @Override
    public String filePreviewHandle(Model model, FileAttribute fileAttribute) {
        String fileName = fileAttribute.getFileViewName();
        List<String> imgUrls = Lists.newArrayList(fileName);
        model.addAttribute("fileRealName", fileAttribute.getFileRealName());
        model.addAttribute("imgurls", imgUrls);
        model.addAttribute("currentUrl", fileName);
        return "picture";
    }

}
