package com.hj.service.fileserver;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.lang3.StringUtils;

import com.hj.config.ConfigConstants;
import com.hj.constants.CommonConstants;
import com.hj.util.SpringUtils;

/**
 * @author dehui dou
 * @date 2020/10/3 18:07
 * @description 文件服务器工厂类
 */
public class FileServerFactory {
    private static Map<String, FileServer> fileServerBeanMap = new ConcurrentHashMap();

    private static final String LOCALSERVER = "localServerImpl";
    private static final String FASTDFSSERVER = "fastDFSServerImpl";
    private static final String MINIOSERVER = "minIOServerImpl";

    public static void init() {
        if (fileServerBeanMap.size() == 0) {
            Map<String, FileServer> beansOfType = SpringUtils.getBeansOfType(FileServer.class);
            fileServerBeanMap.put(LOCALSERVER, beansOfType.get(LOCALSERVER));
            fileServerBeanMap.put(FASTDFSSERVER, beansOfType.get(FASTDFSSERVER));
            fileServerBeanMap.put(MINIOSERVER, beansOfType.get(MINIOSERVER));
        }
    }

    public static FileServer get() {
        String fileServerType = ConfigConstants.getFileServerType();
        if (StringUtils.isNotEmpty(fileServerType) && CommonConstants.FILE_SERVER_FASTDFS.equals(fileServerType)) {
            return fileServerBeanMap.get(FASTDFSSERVER);
        } else if (StringUtils.isNotEmpty(fileServerType) && CommonConstants.FILE_SERVER_MINIO.equals(fileServerType)) {
            return fileServerBeanMap.get(MINIOSERVER);
        } else {
            return fileServerBeanMap.get(LOCALSERVER);
        }
    }
}
