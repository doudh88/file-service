package com.hj.model;

import java.util.List;
import java.util.Map;

/**
 * @author dehui dou
 * @date 2020/10/3 17:54
 * @description 压缩文件类
 */
public class CompressDescFile {
    /**
     * 压缩文件描述信息
     */
    private String fileTree;
    /**
     * 内部文件寻址信息 K->innerFileUniqueId  V->InnerFileInfo
     */
    private List<Map<String, InnerFileInfo>> innerFileList;

    public CompressDescFile() {
    }

    public List<Map<String, InnerFileInfo>> getInnerFileList() {
        return innerFileList;
    }

    public void setInnerFileList(List<Map<String, InnerFileInfo>> innerFileList) {
        this.innerFileList = innerFileList;
    }

    public String getFileTree() {
        return fileTree;
    }

    public void setFileTree(String fileTree) {
        this.fileTree = fileTree;
    }


    public class InnerFileInfo {
        private String fileOrignName;
        private String fileViewName;

        public InnerFileInfo() {
        }

        public InnerFileInfo(String fileOrignName, String fileViewName) {
            this.fileOrignName = fileOrignName;
            this.fileViewName = fileViewName;
        }

        public String getFileOrignName() {
            return fileOrignName;
        }

        public void setFileOrignName(String fileOrignName) {
            this.fileOrignName = fileOrignName;
        }

        public String getFileViewName() {
            return fileViewName;
        }

        public void setFileViewName(String fileViewName) {
            this.fileViewName = fileViewName;
        }
    }
}
