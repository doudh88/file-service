package com.hj.model;

/**
 * @author dehui dou
 * @date 2020/10/3 17:56
 * @description 文件类型，文本，office，压缩包等等
 */
public enum FileType {
    picture("pictureFilePreviewImpl"), compress("compressFilePreviewImpl"), office("officeFilePreviewImpl"),
    simText("simTextFilePreviewImpl"), pdf("pdfFilePreviewImpl"), other("otherFilePreviewImpl"),
    media("mediaFilePreviewImpl"), cad("cadFilePreviewImpl"), compressinner("compressInnerFilePreviewImpl");

    private final String instanceName;

    FileType(String instanceName) {
        this.instanceName = instanceName;
    }

    public String getInstanceName() {
        return instanceName;
    }

}
