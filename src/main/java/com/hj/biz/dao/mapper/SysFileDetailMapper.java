package com.hj.biz.dao.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hj.biz.dao.entity.SysFileDetail;

/**
 * @author dehui dou
 * @date 2020/10/2 14:33
 * @description 文件详情mapper
 */
public interface SysFileDetailMapper extends BaseMapper<SysFileDetail> {

}
