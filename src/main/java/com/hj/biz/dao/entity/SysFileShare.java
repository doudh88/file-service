package com.hj.biz.dao.entity;

import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.TableName;

/**
 * @author dehui dou
 * @date 2020/10/2 14:03
 * @description 文件分享关系表
 */
@TableName("sys_file_share")
public class SysFileShare implements Serializable {

    private static final long serialVersionUID = 1L;
    /** 文件唯一标识 **/
    private String fileUniqueId;

    /** sharesystemcode **/
    private String shareSystemCode;

    public String getFileUniqueId() {
        return fileUniqueId;
    }

    public void setFileUniqueId(String fileUniqueId) {
        this.fileUniqueId = fileUniqueId;
    }

    public String getShareSystemCode() {
        return shareSystemCode;
    }

    public void setShareSystemCode(String shareSystemCode) {
        this.shareSystemCode = shareSystemCode;
    }

    @Override
    public String toString() {
        return "SysFileShare{" + "fileUniqueId=" + fileUniqueId + ", shareSystemCode=" + shareSystemCode + "}";
    }
}
