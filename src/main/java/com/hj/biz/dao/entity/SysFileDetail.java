package com.hj.biz.dao.entity;

import java.io.Serializable;
import java.util.Date;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

/**
 * @author dehui dou
 * @date 2020/10/2 13:58
 * @description 文件详情实体
 */
@TableName("sys_file_detail")
public class SysFileDetail implements Serializable {

    private static final long serialVersionUID = 1L;
    @TableId(value = "ID", type = IdType.INPUT)
    private Long id;

    /** 系统名称 **/
    private String systemCode;

    /** 保存路径 **/
    private String filePath;

    /** 源文件名称 **/
    private String fileName;

    /** 文件格式 **/
    private String format;

    /** 源文件大小 **/
    private String fileSize;

    /** 源文件id **/
    private String fileUniqueId;

    /** 源文件fastdfsid **/
    private String fileFdfsId;

    /** 删除标识 0:可用 1:禁用 **/
    private Integer disabled;

    /** 创建时间 **/
    private Date createTime;

    /** 更新时间 **/
    private Date updateTime;

    /** 删除时间 **/
    private Date deleteTime;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSystemCode() {
        return systemCode;
    }

    public void setSystemCode(String systemCode) {
        this.systemCode = systemCode;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public String getFileSize() {
        return fileSize;
    }

    public void setFileSize(String fileSize) {
        this.fileSize = fileSize;
    }

    public String getFileUniqueId() {
        return fileUniqueId;
    }

    public void setFileUniqueId(String fileUniqueId) {
        this.fileUniqueId = fileUniqueId;
    }

    public String getFileFdfsId() {
        return fileFdfsId;
    }

    public void setFileFdfsId(String fileFdfsId) {
        this.fileFdfsId = fileFdfsId;
    }

    public Integer getDisabled() {
        return disabled;
    }

    public void setDisabled(Integer disabled) {
        this.disabled = disabled;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Date getDeleteTime() {
        return deleteTime;
    }

    public void setDeleteTime(Date deleteTime) {
        this.deleteTime = deleteTime;
    }

    @Override
    public String toString() {
        return "SysFileDetail{" + "id=" + id + ", systemCode=" + systemCode + ", filePath=" + filePath + ", fileName="
            + fileName + ", format=" + format + ", fileSize=" + fileSize + ", fileUniqueId=" + fileUniqueId
            + ", fileFdfsId=" + fileFdfsId + ", disabled=" + disabled + ", createTime=" + createTime + ", updateTime="
            + updateTime + ", deleteTime=" + deleteTime + "}";
    }
}
