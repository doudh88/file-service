package com.hj.biz.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.hj.biz.dao.entity.SysFileDetail;
import com.hj.biz.dao.mapper.SysFileDetailMapper;
import com.hj.constants.CommonConstants.CommonStatusEnum;

/**
 * @author dehui dou
 * @date 2020/9/23 12:11
 * @description 文件详情dao
 */
@Component
public class SysFileDetailDao {
    @Autowired
    private SysFileDetailMapper sysFileDetailMapper;

    /**
     * @author dehui dou
     * @description 根据fileUniqueId获取文件详情
     * @param fileUniqueId
     * @return SysFileDetail
     */
    public SysFileDetail getOneByUniqueId(String fileUniqueId) {
        QueryWrapper<SysFileDetail> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("file_unique_id", fileUniqueId);
        queryWrapper.eq("disabled", CommonStatusEnum.ENABLED);
        List<SysFileDetail> fileList = sysFileDetailMapper.selectList(queryWrapper);
        if (!CollectionUtils.isEmpty(fileList)) {
            return fileList.get(0);
        }
        return null;
    }

    /**
     * @author dehui dou
     * @description
     * @date 2020/9/23 18:42
     * @params [fileUniqueId]
     */
    public SysFileDetail getFileTypeByUniqueId(String fileUniqueId) {
        QueryWrapper<SysFileDetail> queryWrapper = new QueryWrapper<>();
        queryWrapper.select("format");
        queryWrapper.eq("file_unique_id", fileUniqueId);
        queryWrapper.eq("disabled", CommonStatusEnum.ENABLED);
        List<SysFileDetail> fileList = sysFileDetailMapper.selectList(queryWrapper);
        if (!CollectionUtils.isEmpty(fileList)) {
            return fileList.get(0);
        }
        return null;
    }

    /**
     * @author dehui dou
     * @description 新增
     * @date 2020/9/25 9:49
     * @params [sysFileDetail]
     */
    public Integer save(SysFileDetail sysFileDetail) {
        int insert = sysFileDetailMapper.insert(sysFileDetail);
        return insert;
    }
}
