package com.hj.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * @author dehui dou
 * @date 2020/10/3 17:45
 * @description fastdfs配置参数获取
 */
@Component
public class FastDFSConfig {

    @Value("${fastdfs.connect_timeout}")
    private int connectTimeout;
    @Value("${fastdfs.network_timeout}")
    private int networkTimeout;
    @Value("${fastdfs.charset}")
    private String charset;
    @Value("${fastdfs.http.tracker_http_port}")
    private int trackerHttpPort;
    @Value("${fastdfs.http.anti_steal.check_token}")
    private boolean checkToken;
    @Value("${fastdfs.http.secret_key}")
    private String secretKey;
    @Value("${fastdfs.tracker_server}")
    private String trackerServer;

    public int getConnectTimeout() {
        return connectTimeout;
    }

    public int getNetworkTimeout() {
        return networkTimeout;
    }

    public String getCharset() {
        return charset;
    }

    public int getTrackerHttpPort() {
        return trackerHttpPort;
    }

    public boolean getCheckToken() {
        return checkToken;
    }

    public String getSecretKey() {
        return secretKey;
    }

    public String[] getTrackerServer() {
        return trackerServer.split(",");
    }
}
